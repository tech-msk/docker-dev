# docker-dev

## Getting started

- Docker를 활용하여 개발 서버 및 DB서버 환경 구축
- Docker for Windows 내 Ubuntu 서버가 기본 환경

## 환경
- BackEnd
    - WEB Server
        - SSH Server
        - FTP Server
        - Apache
        - Jenkins
    - DB Server
- FrontEnd
    - WAS Server
        - SSH Server
        - FTP Server
        - Tomcat
            - Angular 9
        - Jenkins

## 1. Docker Desktop for Windows 에 Ubuntu 설치

-  커맨드에서 아래 명령어 실행
```
docker pull ubuntu
```

## 2. Ubuntu에 Docker 설치

```
sudo apt-update
sudo apt-upgrade
```
- docker 필수 패키지 설치

```
sudo apt-get install apt-transport-https ca-certificates curl gnupg-agent software-properties-common
```

- docker 설치
```
sudo apt-get update && sudo apt-get install docker-ce docker-ce-cli containerd.io
```

## 3. Ubuntu에 SSH Server 설치

## 4. Ubuntu에 Apache 설치

## 5. Ubuntu에 Tomcat 설치

## 6. Ubuntu에 Jenkins 설치

## 7. Docker에 Postgresql 설치

## 8. Jenkins 배포 환경 만들기
